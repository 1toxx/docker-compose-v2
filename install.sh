#/usr/bin/bash

PLUGINS_DIR="/usr/local/lib/docker/cli-plugins"
COLOR_SUCCESS="\033[1;32m"
COLOR_ERR="\033[1;31m"
COLOR_INFO="\033[1;36m"
COLOR_RESET="\033[0m"

err() {
	echo -e "${COLOR_ERR}[ERROR]${COLOR_RESET} Docker compose installation failed with error code $?."
}

trap err ERR

# Check if docker is installed
command -v docker --version &> /dev/null
[ "$?" -ne "0" ] && echo -e "${COLOR_ERR}[ERROR]${COLOR_RESET} Docker not found. Is the daemon running ?" && exit 1

# Install
echo -e "${COLOR_INFO}[INFO]${COLOR_RESET} Installing to $PLUGINS_DIR/docker-compose..."
sudo mkdir -pv "$PLUGINS_DIR" &&
sudo curl -SL# https://github.com/docker/compose/releases/download/v2.0.1/docker-compose-linux-x86_64 -o "$PLUGINS_DIR/docker-compose" &&
sudo chmod +x "$PLUGINS_DIR/docker-compose" &&
docker compose version &&
echo -e "${COLOR_SUCCESS}[SUCCESS]${COLOR_RESET} Installed."
